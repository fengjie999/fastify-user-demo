import Fastify from 'fastify';
import mysql2 from 'mysql2';

// 创建数据库连接
const connection = mysql2.createConnection({
	host: 'localhost',
	user: 'root',
	password: '123456',
	database: 'myshop'
});

// 连接数据库
connection.connect((err) => {
	if (err) {
		console.error('Error connecting to database: ', err);
		return;
	}
	console.log('Connected to database!');
});

interface UserRegistration {
	username: string;
	password: string;
}

const myUsername = "fengjie";
const myPassword = "123456";

// 创建 Fastify 实例
const server = Fastify({
	logger: true
});

//定义一个路由的根目录
server.get("/", async (req, res) => {
	res.send("Hello Fastify!!!");
})

// 注册路由
server.post<{ Body: UserRegistration }>('/register', async (request, reply) => {
    const { username, password } = request.body;

    try {
        if (username == "") {
            reply.send("用户名不能为空");
        }
		
		const user: any = await queryUser(username);
		if (user && user.status == 1) {
			reply.status(500).send({ error: '用户名经存在' });
		}

        // 在数据库中创建用户
        const result: any = await new Promise((resolve, reject) => {
            connection.query('INSERT INTO user (username, password) VALUES (?,?)', [username, password], (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
            });
        });
		
		console.dir(result);

		if (result.affectedRows > 0) {
			reply.send({ message: '注册成功，用户ID为：' + result.insertId });
		} else {
			reply.status(500).send({ error: '注册失败？' });
		}
    } catch (error) {
        reply.status(500).send({ error: '注册失败' });
    }
});


const queryUser = async (username: string) => {
	return new Promise((resolve, reject) => {
		connection.query('SELECT * FROM user WHERE username =?', [username], (err, results: any[], fields) => {
		if (err) {
			reject(err);
		} else {
			resolve(results[0]);
		}
		});
	});
};

server.post<{ Body: UserRegistration }>('/login', async (request, reply) => {
	const { username, password } = request.body;
	try {
		const user: any = await queryUser(username);
		if (!user) {
			reply.status(401).send({ error: '用户不存在' });
		}
		if (user.password === password) {
			reply.send({ message: '登录成功' });
		} else {
			reply.status(401).send({ error: '密码错误' });
		}
	} catch (err) {
		console.error('Error querying database: ', err);
		reply.status(401).send({ error: err });
	}
});

server.get('/user/list', async (request, reply) => {
    try {
        var users: any;
        users = await new Promise((resolve, reject) => {
            connection.query('SELECT * FROM user', (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
            });
        });
        reply.send(users);
    } catch (error) {
        reply.status(500).send({ error: '获取用户列表失败' });
    }
});

server.put<{ Params: { id: string }, Body: UserRegistration }>('/user/update/:id', async (request, reply) => {
    const { id } = request.params;
    const { username, password } = request.body;

    try {
		const result: any = await new Promise((resolve, reject) => {
			// 更新用户信息
			connection.query('UPDATE user SET username =?, password =? WHERE id =?', [username, password, id], (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
			});
		});

		if (result.affectedRows > 0) {
			reply.send({ message: '用户信息更新成功' });
		} else {
			reply.status(500).send({ error: '用户信息更新失败???' });
		}
    } catch (error) {
        reply.status(500).send({ error: '用户信息更新失败' });
    }
});

// 启动服务器
const start = async () => {
	try {
		await server.listen(3000);
	} catch (err) {
		server.log.error(err);
		process.exit(1);
	}
};

start();